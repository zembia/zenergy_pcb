EESchema Schematic File Version 4
LIBS:Gateway-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gateway-rescue:TXB0108PWR-zembia U3
U 1 1 5D1400CE
P 2600 7300
F 0 "U3" H 2875 8587 60  0000 C CNN
F 1 "TXB0108PWR" H 2875 8481 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-20_4.4x6.5mm_Pitch0.65mm" H 2500 7200 60  0001 C CNN
F 3 "" H 2600 7300 60  0001 C CNN
F 4 "TXB0108PWR" H 2700 7400 60  0001 C CNN "MPN"
	1    2600 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2775 1425 2775 1400
Wire Wire Line
	2775 800  2975 800 
Connection ~ 2975 800 
$Comp
L Gateway-rescue:CP-device C5
U 1 1 5D1401FA
P 3775 1000
F 0 "C5" H 3893 1046 50  0000 L CNN
F 1 "2.2uF" H 3893 955 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Reflow" H 3813 850 50  0001 C CNN
F 3 "" H 3775 1000 50  0001 C CNN
F 4 "T494A225K016AT" H 3775 1000 60  0001 C CNN "MPN"
	1    3775 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3775 800  3775 850 
Wire Wire Line
	2975 800  3775 800 
Wire Wire Line
	3775 1150 3775 1250
Text Label 3775 1250 3    60   ~ 0
GND
$Comp
L Gateway-rescue:C-device C7
U 1 1 5D140342
P 4300 1000
F 0 "C7" H 4415 1046 50  0000 L CNN
F 1 "33pF" H 4415 955 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4338 850 50  0001 C CNN
F 3 "" H 4300 1000 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 4300 1000 60  0001 C CNN "MPN"
	1    4300 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3775 800  4300 800 
Wire Wire Line
	4300 800  4300 850 
Connection ~ 3775 800 
Wire Wire Line
	4300 1150 4300 1250
Text Label 4300 1250 3    60   ~ 0
GND
$Comp
L Gateway-rescue:Ferrite_Bead_Small-device L1
U 1 1 5D1406B4
P 4700 800
F 0 "L1" V 4463 800 50  0000 C CNN
F 1 "100 ohm 2A" V 4554 800 50  0000 C CNN
F 2 "Inductors_SMD:L_0603" V 4630 800 50  0001 C CNN
F 3 "" H 4700 800 50  0001 C CNN
F 4 "BLM18EG101TN1D" V 4700 800 60  0001 C CNN "MPN"
	1    4700 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 800  4600 800 
Connection ~ 4300 800 
$Comp
L Gateway-rescue:C-device C8
U 1 1 5D140825
P 4975 1000
F 0 "C8" H 5090 1046 50  0000 L CNN
F 1 "33pF" H 5090 955 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5013 850 50  0001 C CNN
F 3 "" H 4975 1000 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 4975 1000 60  0001 C CNN "MPN"
	1    4975 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 800  4975 800 
Wire Wire Line
	4975 800  4975 850 
Text Label 4975 1250 3    60   ~ 0
GND
Wire Wire Line
	4975 1150 4975 1250
$Comp
L Gateway-rescue:CP-device C10
U 1 1 5D14092A
P 5450 1000
F 0 "C10" H 5568 1046 50  0000 L CNN
F 1 "100uF" H 5568 955 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-6032-28_Kemet-C" H 5488 850 50  0001 C CNN
F 3 "" H 5450 1000 50  0001 C CNN
F 4 "T494D107K016AT" H 5450 1000 60  0001 C CNN "MPN"
	1    5450 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 800  5450 800 
Wire Wire Line
	5450 800  5450 850 
Connection ~ 4975 800 
Text Label 5450 1250 3    60   ~ 0
GND
Wire Wire Line
	5450 1250 5450 1150
Wire Wire Line
	5450 800  5975 800 
Connection ~ 5450 800 
Text HLabel 5975 800  2    60   Input ~ 0
GPRS_VCC
$Comp
L Gateway-rescue:Molex_104642-zembia-zembia U5
U 1 1 5D141C3C
P 7300 2075
F 0 "U5" H 7300 3062 60  0000 C CNN
F 1 "Molex_104642-zembia" H 7300 2956 60  0000 C CNN
F 2 "FootprintsZembia:Molex_104642" H 7250 3175 60  0001 C CNN
F 3 "" H 7250 3175 60  0001 C CNN
F 4 "104642-1610" H 7300 2075 60  0001 C CNN "MPN"
	1    7300 2075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1375 6375 1375
Wire Wire Line
	6375 1375 6375 1475
Wire Wire Line
	6375 2075 6550 2075
Wire Wire Line
	6550 1975 6375 1975
Connection ~ 6375 1975
Wire Wire Line
	6375 1975 6375 2075
Wire Wire Line
	6550 1875 6375 1875
Connection ~ 6375 1875
Wire Wire Line
	6375 1875 6375 1975
Wire Wire Line
	6550 1775 6375 1775
Connection ~ 6375 1775
Wire Wire Line
	6375 1775 6375 1875
Wire Wire Line
	6375 1675 6550 1675
Connection ~ 6375 1675
Wire Wire Line
	6375 1675 6375 1775
Wire Wire Line
	6550 1575 6375 1575
Connection ~ 6375 1575
Wire Wire Line
	6375 1575 6375 1675
Wire Wire Line
	6550 1475 6375 1475
Connection ~ 6375 1475
Wire Wire Line
	6375 1475 6375 1575
Text Label 6375 1375 2    60   ~ 0
GND
$Comp
L Gateway-rescue:C-device C9
U 1 1 5D142D8D
P 5200 2975
F 0 "C9" H 5315 3021 50  0000 L CNN
F 1 "33pF" H 5315 2930 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5238 2825 50  0001 C CNN
F 3 "" H 5200 2975 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 5200 2975 60  0001 C CNN "MPN"
	1    5200 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2275 5200 2275
$Comp
L Gateway-rescue:C-device C13
U 1 1 5D143414
P 6425 2975
F 0 "C13" H 6540 3021 50  0000 L CNN
F 1 "33pF" H 6540 2930 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6463 2825 50  0001 C CNN
F 3 "" H 6425 2975 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 6425 2975 60  0001 C CNN "MPN"
	1    6425 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2775 6425 2775
Wire Wire Line
	6425 2775 6425 2825
$Comp
L Gateway-rescue:C-device C12
U 1 1 5D1438F6
P 6025 2975
F 0 "C12" H 6140 3021 50  0000 L CNN
F 1 "33pF" H 6140 2930 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6063 2825 50  0001 C CNN
F 3 "" H 6025 2975 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 6025 2975 60  0001 C CNN "MPN"
	1    6025 2975
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:C-device C11
U 1 1 5D143E08
P 5600 2975
F 0 "C11" H 5715 3021 50  0000 L CNN
F 1 "33pF" H 5715 2930 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5638 2825 50  0001 C CNN
F 3 "" H 5600 2975 50  0001 C CNN
F 4 "C0603C330K5RACTU" H 5600 2975 60  0001 C CNN "MPN"
	1    5600 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2375 5600 2375
Wire Wire Line
	5600 2375 5600 2825
Wire Wire Line
	6025 2825 6025 2475
Wire Wire Line
	6025 2475 6550 2475
Wire Wire Line
	6550 2575 6225 2575
Text Label 6225 2575 0    60   ~ 0
GND
Wire Wire Line
	5200 2275 5200 2825
Text Label 5200 3325 1    60   ~ 0
GND
Wire Wire Line
	5200 3325 5200 3125
Text Label 5600 3325 1    60   ~ 0
GND
Wire Wire Line
	5600 3325 5600 3125
Text Label 6025 3325 1    60   ~ 0
GND
Wire Wire Line
	6025 3325 6025 3125
Text Label 6425 3325 1    60   ~ 0
GND
Wire Wire Line
	6425 3325 6425 3125
Text Label 5200 2275 0    60   ~ 0
SIM_VCC
Text Label 5600 2375 0    60   ~ 0
SIM_RST
Text Label 6025 2475 0    60   ~ 0
SIM_CLK
Text Label 6425 2775 2    60   ~ 0
SIM_IO
Text Label 1575 2325 0    60   ~ 0
SIM_VCC
Wire Wire Line
	1575 2325 1975 2325
Text Label 1575 2425 0    60   ~ 0
SIM_RST
Wire Wire Line
	1575 2425 1975 2425
Text Label 1575 2525 0    60   ~ 0
SIM_CLK
Wire Wire Line
	1575 2525 1975 2525
Text Label 1575 2625 0    60   ~ 0
SIM_IO
Wire Wire Line
	1575 2625 1975 2625
Wire Wire Line
	1925 7200 2400 7200
Wire Wire Line
	2400 6500 1925 6500
Text Label 1925 6500 0    60   ~ 0
RXD_GPRS
Text Label 1925 6600 0    60   ~ 0
TXD_GPRS
Wire Wire Line
	1925 6600 2400 6600
Wire Wire Line
	1925 6300 2400 6300
Text Label 1525 4825 0    60   ~ 0
RXD_GPRS
Wire Wire Line
	1525 4825 1975 4825
Text Label 1525 4725 0    60   ~ 0
TXD_GPRS
Wire Wire Line
	1525 4725 1975 4725
Text Label 1525 4525 0    60   ~ 0
RTS_GPRS
Wire Wire Line
	1525 4525 1975 4525
Text Label 1525 4625 0    60   ~ 0
CTS_GPRS
Wire Wire Line
	1525 4625 1975 4625
Text Label 1525 4425 0    60   ~ 0
DTR_GPRS
Text Label 1925 6700 0    60   ~ 0
CTS_GPRS
Wire Wire Line
	1925 6700 2400 6700
Text Label 1925 6800 0    60   ~ 0
RTS_GPRS
Wire Wire Line
	1925 6800 2400 6800
Text Label 1925 6300 0    60   ~ 0
DTR_GPRS
Text Label 2775 5675 1    60   ~ 0
GND
Text Label 2975 5675 1    60   ~ 0
GND
Wire Wire Line
	2975 5425 2975 5675
NoConn ~ 8050 1925
NoConn ~ 8050 2025
NoConn ~ 6550 2675
Wire Wire Line
	8050 2325 8650 2325
Text Label 8650 2325 2    60   ~ 0
GND
NoConn ~ 8050 1725
$Comp
L Gateway-rescue:C-device C4
U 1 1 5D16DBED
P 1525 6225
F 0 "C4" H 1640 6271 50  0000 L CNN
F 1 "0.1uF" H 1640 6180 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1563 6075 50  0001 C CNN
F 3 "" H 1525 6225 50  0001 C CNN
F 4 "C0603C104M4RAC7411" H 1525 6225 60  0001 C CNN "MPN"
	1    1525 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 6400 1525 6375
Wire Wire Line
	1525 6400 2400 6400
Text Label 1525 6025 1    60   ~ 0
GND
Wire Wire Line
	1525 6025 1525 6075
Text Label 3575 7200 2    60   ~ 0
GND
Text HLabel 3975 6500 2    60   Output ~ 0
RXD_GPRS_MCU
Text HLabel 3975 6600 2    60   Input ~ 0
TXD_GPRS_MCU
Text Label 800  650  2    60   ~ 0
GND
Wire Wire Line
	800  650  1325 650 
Text GLabel 1325 650  2    60   Input ~ 0
GND
$Comp
L birdchile-rescue:GS2-Connector J4
U 1 1 5D1785AA
P 1150 4425
F 0 "J4" V 945 4425 50  0000 C CNN
F 1 "GS2" V 1036 4425 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" V 1224 4425 50  0001 C CNN
F 3 "" H 1150 4425 50  0001 C CNN
	1    1150 4425
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 4425 1975 4425
Text Label 950  4425 2    60   ~ 0
GND
Text Notes 525  4650 0    60   ~ 0
short jumper to skip \npython load on start
$Comp
L Gateway-rescue:C-device C6
U 1 1 5D17C441
P 3900 6250
F 0 "C6" H 4015 6296 50  0000 L CNN
F 1 "0.1uF" H 4015 6205 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3938 6100 50  0001 C CNN
F 3 "" H 3900 6250 50  0001 C CNN
F 4 "C0603C104M4RAC7411" H 3900 6250 60  0001 C CNN "MPN"
	1    3900 6250
	1    0    0    -1  
$EndComp
Text Label 3900 6000 1    60   ~ 0
GND
Wire Wire Line
	3900 6000 3900 6100
Wire Wire Line
	3350 6700 3975 6700
Text HLabel 3975 6700 2    60   Output ~ 0
CTS_MCU
Wire Wire Line
	3350 6800 3975 6800
Wire Wire Line
	3350 6300 3650 6300
Text HLabel 3650 6300 1    60   Input ~ 0
DTR_MCU
Text HLabel 3975 6800 2    60   Input ~ 0
RTS_MCU
Wire Wire Line
	1975 3825 1525 3825
Wire Wire Line
	1975 3925 1525 3925
Text Label 1525 3825 0    60   ~ 0
RXD_AUX
Text Label 1525 3925 0    60   ~ 0
TXD_AUX
Text Label 1925 6900 0    60   ~ 0
RXD_AUX
Wire Wire Line
	1925 6900 2400 6900
Text Label 1925 7000 0    60   ~ 0
TXD_AUX
Wire Wire Line
	1925 7000 2400 7000
Text HLabel 3975 6900 2    60   Input ~ 0
RXD_AUX_IN
Text HLabel 4000 7000 2    60   Output ~ 0
TXD_AUX_OUT
Wire Wire Line
	3775 3725 4050 3725
Wire Wire Line
	4200 3925 4200 4100
Text Label 4200 4100 2    60   ~ 0
GND
$Comp
L Switch:SW_Push SW1
U 1 1 5D19927E
P 1425 1725
F 0 "SW1" H 1425 2010 50  0000 C CNN
F 1 "SW_Push" H 1425 1919 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_TL3342" H 1425 1925 50  0001 C CNN
F 3 "" H 1425 1925 50  0001 C CNN
F 4 "PTS525SM08SMTR2LFS" H 1425 1725 60  0001 C CNN "MPN"
	1    1425 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1225 1725 850  1725
Text Label 850  1725 2    60   ~ 0
GND
Text Label 1750 7100 0    60   ~ 0
RST_CTRL_MCU
Wire Wire Line
	1750 7100 2400 7100
Wire Wire Line
	3350 7100 4000 7100
Text HLabel 4000 7100 2    60   Input ~ 0
Telit_RST_Ctrl
$Comp
L birdchile-rescue:Test_Point-Connector TP4
U 1 1 5D1B6A59
P 4525 2300
F 0 "TP4" V 4525 3000 50  0000 C CNN
F 1 "Test_Point" V 4525 2725 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4725 2300 50  0001 C CNN
F 3 "~" H 4725 2300 50  0001 C CNN
	1    4525 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 2325 4525 2300
Wire Wire Line
	3775 2325 4525 2325
$Comp
L Connector:TestPoint TP1
U 1 1 5D1BA158
P 2600 1325
F 0 "TP1" V 2600 2025 50  0000 C CNN
F 1 "Test_Point" V 2600 1750 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 2800 1325 50  0001 C CNN
F 3 "~" H 2800 1325 50  0001 C CNN
	1    2600 1325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1325 2600 1400
Wire Wire Line
	2600 1400 2775 1400
Connection ~ 2775 1400
$Comp
L Connector:TestPoint TP2
U 1 1 5D1BD956
P 2625 5700
F 0 "TP2" V 2625 6400 50  0000 C CNN
F 1 "Test_Point" V 2625 6125 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 2825 5700 50  0001 C CNN
F 3 "~" H 2825 5700 50  0001 C CNN
	1    2625 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2625 5700 2775 5700
Wire Wire Line
	2775 5425 2775 5700
Text GLabel 1325 775  2    60   Input ~ 0
3V3
Wire Wire Line
	1325 775  800  775 
Text Label 800  775  2    60   ~ 0
3V3
Text Label 4175 6400 0    60   ~ 0
3V3
Text GLabel 1325 925  2    60   Input ~ 0
1V8
Wire Wire Line
	1325 925  800  925 
Text Label 800  925  2    60   ~ 0
1V8
Text Label 1925 6400 0    60   ~ 0
1V8
Text Label 1925 7200 0    60   ~ 0
1V8
Wire Wire Line
	3350 6900 4900 6900
$Comp
L birdchile-rescue:Test_Point-Connector TP7
U 1 1 5D218A67
P 4900 6900
F 0 "TP7" V 4900 7600 50  0000 C CNN
F 1 "Test_Point" V 4900 7325 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 5100 6900 50  0001 C CNN
F 3 "~" H 5100 6900 50  0001 C CNN
	1    4900 6900
	1    0    0    -1  
$EndComp
$Comp
L birdchile-rescue:Test_Point-Connector TP8
U 1 1 5D219855
P 5000 6900
F 0 "TP8" V 5000 7600 50  0000 C CNN
F 1 "Test_Point" V 5000 7325 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 5200 6900 50  0001 C CNN
F 3 "~" H 5200 6900 50  0001 C CNN
	1    5000 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7000 5000 6900
Wire Wire Line
	3350 7000 5000 7000
$Comp
L birdchile-rescue:Test_Point-Connector TP5
U 1 1 5D2B8693
P 4625 6250
F 0 "TP5" V 4625 6950 50  0000 C CNN
F 1 "Test_Point" V 4625 6675 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4825 6250 50  0001 C CNN
F 3 "~" H 4825 6250 50  0001 C CNN
	1    4625 6250
	1    0    0    -1  
$EndComp
$Comp
L birdchile-rescue:Test_Point-Connector TP6
U 1 1 5D2B8CE5
P 4725 6250
F 0 "TP6" V 4725 6950 50  0000 C CNN
F 1 "Test_Point" V 4725 6675 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4925 6250 50  0001 C CNN
F 3 "~" H 4925 6250 50  0001 C CNN
	1    4725 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4625 6500 4625 6250
Wire Wire Line
	3350 6500 4625 6500
Wire Wire Line
	4725 6600 4725 6250
Wire Wire Line
	3350 6600 4725 6600
$Comp
L birdchile-rescue:Test_Point-Connector TP3
U 1 1 5D2C39E7
P 3950 7225
F 0 "TP3" V 3950 7925 50  0000 C CNN
F 1 "Test_Point" V 3950 7650 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4150 7225 50  0001 C CNN
F 3 "~" H 4150 7225 50  0001 C CNN
	1    3950 7225
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 7200 3950 7225
Wire Wire Line
	3350 7200 3950 7200
NoConn ~ 1975 3725
NoConn ~ 1975 5025
NoConn ~ 1975 5125
NoConn ~ 1975 4125
NoConn ~ 1975 4225
NoConn ~ 1975 4325
NoConn ~ 3775 5125
NoConn ~ 3775 2225
$Comp
L birdchile-rescue:Test_Point-Connector TP9
U 1 1 5D7BFA8D
P 5675 1625
F 0 "TP9" V 5675 2325 50  0000 C CNN
F 1 "Test_Point" V 5675 2050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 5875 1625 50  0001 C CNN
F 3 "~" H 5875 1625 50  0001 C CNN
	1    5675 1625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5675 1625 5900 1625
Text Label 5900 1625 1    60   ~ 0
GND
Wire Wire Line
	1625 1725 1975 1725
$Comp
L birdchile-rescue:Conn_Coaxial-Connector J5
U 1 1 5D193A6F
P 4200 3725
F 0 "J5" H 4299 3701 50  0000 L CNN
F 1 "Conn_Coaxial" H 4299 3610 50  0000 L CNN
F 2 "FootprintsZembia:U.FL-R-SMT-1(10)" H 4200 3725 50  0001 C CNN
F 3 "" H 4200 3725 50  0001 C CNN
F 4 "U.FL-R-SMT-1(10)" H 4200 3725 60  0001 C CNN "MPN"
	1    4200 3725
	1    0    0    -1  
$EndComp
NoConn ~ 8050 2125
NoConn ~ 8050 1825
NoConn ~ 8050 1625
NoConn ~ 8050 2425
NoConn ~ 8050 2525
Wire Wire Line
	3350 6400 3900 6400
Wire Wire Line
	3900 6400 4175 6400
Connection ~ 3900 6400
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18DB3
P 9925 2125
AR Path="/5DC18DB3" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DB3" Ref="D1"  Part="1" 
F 0 "D1" H 9918 2341 50  0000 C CNN
F 1 "LED" H 9918 2250 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 2125 50  0001 C CNN
F 3 "~" H 9925 2125 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 2125 50  0001 C CNN "MPN"
	1    9925 2125
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18DBA
P 10325 2125
AR Path="/5DC18DBA" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DBA" Ref="R1"  Part="1" 
F 0 "R1" V 10118 2125 50  0000 C CNN
F 1 "120" V 10209 2125 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 2125 50  0001 C CNN
F 3 "~" H 10325 2125 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 2125 50  0001 C CNN "MPN"
	1    10325 2125
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 2125 10175 2125
Text Label 10850 2975 0    60   ~ 0
3V3
Wire Wire Line
	10475 2125 10850 2125
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18DC4
P 9425 2025
AR Path="/5DC18DC4" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DC4" Ref="Q1"  Part="1" 
F 0 "Q1" V 9660 2025 50  0000 C CNN
F 1 "MMBT3904" V 9751 2025 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 1950 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 2025 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 2025 50  0001 C CNN "MPN"
	1    9425 2025
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 2125 9775 2125
Text Label 9225 2125 1    50   ~ 0
GND
Wire Wire Line
	10850 2125 10850 2800
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18DCE
P 9925 2800
AR Path="/5DC18DCE" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DCE" Ref="D2"  Part="1" 
F 0 "D2" H 9918 3016 50  0000 C CNN
F 1 "LED" H 9918 2925 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 2800 50  0001 C CNN
F 3 "~" H 9925 2800 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 2800 50  0001 C CNN "MPN"
	1    9925 2800
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18DD5
P 10325 2800
AR Path="/5DC18DD5" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DD5" Ref="R2"  Part="1" 
F 0 "R2" V 10118 2800 50  0000 C CNN
F 1 "120" V 10209 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 2800 50  0001 C CNN
F 3 "~" H 10325 2800 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 2800 50  0001 C CNN "MPN"
	1    10325 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 2800 10175 2800
Wire Wire Line
	10475 2800 10850 2800
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18DDE
P 9425 2700
AR Path="/5DC18DDE" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DDE" Ref="Q2"  Part="1" 
F 0 "Q2" V 9660 2700 50  0000 C CNN
F 1 "MMBT3904" V 9751 2700 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 2700 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 2700 50  0001 C CNN "MPN"
	1    9425 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 2800 9775 2800
Text Label 9225 2800 1    50   ~ 0
GND
Connection ~ 10850 2800
Wire Wire Line
	10850 2800 10850 3375
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18DE9
P 9925 3375
AR Path="/5DC18DE9" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DE9" Ref="D3"  Part="1" 
F 0 "D3" H 9918 3591 50  0000 C CNN
F 1 "LED" H 9918 3500 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 3375 50  0001 C CNN
F 3 "~" H 9925 3375 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 3375 50  0001 C CNN "MPN"
	1    9925 3375
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18DF0
P 10325 3375
AR Path="/5DC18DF0" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DF0" Ref="R3"  Part="1" 
F 0 "R3" V 10118 3375 50  0000 C CNN
F 1 "120" V 10209 3375 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 3375 50  0001 C CNN
F 3 "~" H 10325 3375 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 3375 50  0001 C CNN "MPN"
	1    10325 3375
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 3375 10175 3375
Wire Wire Line
	10475 3375 10850 3375
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18DF9
P 9425 3275
AR Path="/5DC18DF9" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18DF9" Ref="Q3"  Part="1" 
F 0 "Q3" V 9660 3275 50  0000 C CNN
F 1 "MMBT3904" V 9751 3275 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 3200 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 3275 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 3275 50  0001 C CNN "MPN"
	1    9425 3275
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 3375 9775 3375
Text Label 9225 3375 1    50   ~ 0
GND
Connection ~ 10850 3375
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18E03
P 9925 4000
AR Path="/5DC18E03" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E03" Ref="D4"  Part="1" 
F 0 "D4" H 9918 4216 50  0000 C CNN
F 1 "LED" H 9918 4125 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 4000 50  0001 C CNN
F 3 "~" H 9925 4000 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 4000 50  0001 C CNN "MPN"
	1    9925 4000
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18E0A
P 10325 4000
AR Path="/5DC18E0A" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E0A" Ref="R4"  Part="1" 
F 0 "R4" V 10118 4000 50  0000 C CNN
F 1 "120" V 10209 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 4000 50  0001 C CNN
F 3 "~" H 10325 4000 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 4000 50  0001 C CNN "MPN"
	1    10325 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 4000 10175 4000
Wire Wire Line
	10475 4000 10850 4000
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18E13
P 9425 3900
AR Path="/5DC18E13" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E13" Ref="Q4"  Part="1" 
F 0 "Q4" V 9660 3900 50  0000 C CNN
F 1 "MMBT3904" V 9751 3900 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 3825 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 3900 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 3900 50  0001 C CNN "MPN"
	1    9425 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 4000 9775 4000
Text Label 9225 4000 1    50   ~ 0
GND
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18E1C
P 9925 4600
AR Path="/5DC18E1C" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E1C" Ref="D5"  Part="1" 
F 0 "D5" H 9918 4816 50  0000 C CNN
F 1 "LED" H 9918 4725 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 4600 50  0001 C CNN
F 3 "~" H 9925 4600 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 4600 50  0001 C CNN "MPN"
	1    9925 4600
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18E23
P 10325 4600
AR Path="/5DC18E23" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E23" Ref="R5"  Part="1" 
F 0 "R5" V 10118 4600 50  0000 C CNN
F 1 "120" V 10209 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 4600 50  0001 C CNN
F 3 "~" H 10325 4600 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 4600 50  0001 C CNN "MPN"
	1    10325 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 4600 10175 4600
Wire Wire Line
	10475 4600 10850 4600
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18E2C
P 9425 4500
AR Path="/5DC18E2C" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E2C" Ref="Q5"  Part="1" 
F 0 "Q5" V 9660 4500 50  0000 C CNN
F 1 "MMBT3904" V 9751 4500 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 4425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 4500 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 4500 50  0001 C CNN "MPN"
	1    9425 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 4600 9775 4600
Text Label 9225 4600 1    50   ~ 0
GND
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18E35
P 9925 5275
AR Path="/5DC18E35" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E35" Ref="D6"  Part="1" 
F 0 "D6" H 9918 5491 50  0000 C CNN
F 1 "LED" H 9918 5400 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 5275 50  0001 C CNN
F 3 "~" H 9925 5275 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 5275 50  0001 C CNN "MPN"
	1    9925 5275
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18E3C
P 10325 5275
AR Path="/5DC18E3C" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E3C" Ref="R6"  Part="1" 
F 0 "R6" V 10118 5275 50  0000 C CNN
F 1 "120" V 10209 5275 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 5275 50  0001 C CNN
F 3 "~" H 10325 5275 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 5275 50  0001 C CNN "MPN"
	1    10325 5275
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 5275 10175 5275
Wire Wire Line
	10475 5275 10850 5275
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18E45
P 9425 5175
AR Path="/5DC18E45" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E45" Ref="Q6"  Part="1" 
F 0 "Q6" V 9660 5175 50  0000 C CNN
F 1 "MMBT3904" V 9751 5175 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 5100 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 5175 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 5175 50  0001 C CNN "MPN"
	1    9425 5175
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 5275 9775 5275
Text Label 9225 5275 1    50   ~ 0
GND
$Comp
L Gateway-rescue:LED-device D?
U 1 1 5DC18E4E
P 9925 5850
AR Path="/5DC18E4E" Ref="D?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E4E" Ref="D7"  Part="1" 
F 0 "D7" H 9918 6066 50  0000 C CNN
F 1 "LED" H 9918 5975 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9925 5850 50  0001 C CNN
F 3 "~" H 9925 5850 50  0001 C CNN
F 4 "749-SM0805UYC" H 9925 5850 50  0001 C CNN "MPN"
	1    9925 5850
	1    0    0    -1  
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DC18E55
P 10325 5850
AR Path="/5DC18E55" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E55" Ref="R7"  Part="1" 
F 0 "R7" V 10118 5850 50  0000 C CNN
F 1 "120" V 10209 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10255 5850 50  0001 C CNN
F 3 "~" H 10325 5850 50  0001 C CNN
F 4 "CR0603-JW-121ELF" V 10325 5850 50  0001 C CNN "MPN"
	1    10325 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	10075 5850 10175 5850
Wire Wire Line
	10475 5850 10850 5850
$Comp
L Transistor_BJT:MMBT3904 Q?
U 1 1 5DC18E5E
P 9425 5750
AR Path="/5DC18E5E" Ref="Q?"  Part="1" 
AR Path="/5DAFB8E1/5DC18E5E" Ref="Q7"  Part="1" 
F 0 "Q7" V 9660 5750 50  0000 C CNN
F 1 "MMBT3904" V 9751 5750 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9625 5675 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9425 5750 50  0001 L CNN
F 4 "MMBT3904-TP" V 9425 5750 50  0001 C CNN "MPN"
	1    9425 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 5850 9775 5850
Text Label 9225 5850 1    50   ~ 0
GND
Wire Wire Line
	10850 3375 10850 4000
Wire Wire Line
	10850 4000 10850 4600
Connection ~ 10850 4000
Connection ~ 10850 4600
Wire Wire Line
	10850 4600 10850 5275
Connection ~ 10850 5275
Wire Wire Line
	10850 5275 10850 5850
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB11C1B
P 1425 3425
AR Path="/5DB11C1B" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB11C1B" Ref="R12"  Part="1" 
F 0 "R12" V 1218 3425 50  0000 C CNN
F 1 "1k" V 1309 3425 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 3425 50  0001 C CNN
F 3 "~" H 1425 3425 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 3425 50  0001 C CNN "MPN"
	1    1425 3425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 3425 1575 3425
Wire Wire Line
	1275 3425 1150 3425
Text Label 1150 3425 2    50   ~ 0
LED1
Text Label 9425 1825 2    50   ~ 0
LED1
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB2A093
P 1425 3325
AR Path="/5DB2A093" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB2A093" Ref="R11"  Part="1" 
F 0 "R11" V 1218 3325 50  0000 C CNN
F 1 "1k" V 1309 3325 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 3325 50  0001 C CNN
F 3 "~" H 1425 3325 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 3325 50  0001 C CNN "MPN"
	1    1425 3325
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 3325 1575 3325
Wire Wire Line
	1275 3325 1150 3325
Text Label 1150 3325 2    50   ~ 0
LED2
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB2FDBB
P 1425 3225
AR Path="/5DB2FDBB" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB2FDBB" Ref="R10"  Part="1" 
F 0 "R10" V 1218 3225 50  0000 C CNN
F 1 "1k" V 1309 3225 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 3225 50  0001 C CNN
F 3 "~" H 1425 3225 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 3225 50  0001 C CNN "MPN"
	1    1425 3225
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 3225 1575 3225
Wire Wire Line
	1275 3225 1150 3225
Text Label 1150 3225 2    50   ~ 0
LED3
Text Label 9425 2500 2    50   ~ 0
LED2
Text Label 9425 3075 2    50   ~ 0
LED3
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB45495
P 1425 3125
AR Path="/5DB45495" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB45495" Ref="R13"  Part="1" 
F 0 "R13" V 1218 3125 50  0000 C CNN
F 1 "1k" V 1309 3125 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 3125 50  0001 C CNN
F 3 "~" H 1425 3125 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 3125 50  0001 C CNN "MPN"
	1    1425 3125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 3125 1575 3125
Wire Wire Line
	1275 3125 1150 3125
Text Label 1150 3125 2    50   ~ 0
LED4
Text Label 9425 3700 2    50   ~ 0
LED4
Wire Wire Line
	2975 1425 2975 800 
Wire Wire Line
	2775 1400 2775 800 
$Comp
L RF_GSM:UL865 U4
U 1 1 5D13FF1A
P 2875 3225
F 0 "U4" H 2875 5203 50  0000 C CNN
F 1 "UL865" H 2875 5112 50  0000 C CNN
F 2 "RF_Modules:Telit_xL865" H 2875 625 50  0001 C CNN
F 3 "http://www.telit.com/fileadmin/user_upload/products/Downloads/3G/Telit_UL865_Hardware_User_Guide_r8.pdf" H 2875 2725 50  0001 C CNN
F 4 "UL865NAD204T701" H 2875 3225 50  0001 C CNN "MPN"
	1    2875 3225
	1    0    0    -1  
$EndComp
NoConn ~ 1975 3525
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB66A24
P 1425 3025
AR Path="/5DB66A24" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB66A24" Ref="R16"  Part="1" 
F 0 "R16" V 1218 3025 50  0000 C CNN
F 1 "1k" V 1309 3025 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 3025 50  0001 C CNN
F 3 "~" H 1425 3025 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 3025 50  0001 C CNN "MPN"
	1    1425 3025
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 3025 1575 3025
Wire Wire Line
	1275 3025 1150 3025
Text Label 1150 3025 2    50   ~ 0
LED5
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB6D738
P 1425 2925
AR Path="/5DB6D738" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB6D738" Ref="R15"  Part="1" 
F 0 "R15" V 1218 2925 50  0000 C CNN
F 1 "1k" V 1309 2925 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 2925 50  0001 C CNN
F 3 "~" H 1425 2925 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 2925 50  0001 C CNN "MPN"
	1    1425 2925
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 2925 1575 2925
Wire Wire Line
	1275 2925 1150 2925
Text Label 1150 2925 2    50   ~ 0
LED6
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB74495
P 1425 2825
AR Path="/5DB74495" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB74495" Ref="R14"  Part="1" 
F 0 "R14" V 1218 2825 50  0000 C CNN
F 1 "1k" V 1309 2825 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1355 2825 50  0001 C CNN
F 3 "~" H 1425 2825 50  0001 C CNN
F 4 "CR0603-JW-102ELF" V 1425 2825 50  0001 C CNN "MPN"
	1    1425 2825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1975 2825 1575 2825
Wire Wire Line
	1275 2825 1150 2825
Text Label 1150 2825 2    50   ~ 0
LED7
Text Label 9425 4300 2    50   ~ 0
LED5
Text Label 9425 4975 2    50   ~ 0
LED6
Text Label 9425 5550 2    50   ~ 0
LED7
$Comp
L Connector:USB_C_Receptacle_USB2.0 J6
U 1 1 5DB28BDC
P 6500 4575
F 0 "J6" H 6607 5442 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 6607 5351 50  0000 C CNN
F 2 "FootprintsZembia:CX90M-16P" H 6650 4575 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 6650 4575 50  0001 C CNN
F 4 "CX90M-16P" H 6500 4575 50  0001 C CNN "MPN"
	1    6500 4575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3975 7325 3975
Wire Wire Line
	1575 1925 1975 1925
Text Label 1575 1925 0    50   ~ 0
VBUS
Wire Wire Line
	1575 2025 1975 2025
Wire Wire Line
	1575 2125 1975 2125
Text Label 1575 2125 0    50   ~ 0
D+
Text Label 1575 2025 0    50   ~ 0
D-
Text Label 7325 3975 0    50   ~ 0
VBUS
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB47FDE
P 7450 4175
AR Path="/5DB47FDE" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB47FDE" Ref="R17"  Part="1" 
F 0 "R17" V 7243 4175 50  0000 C CNN
F 1 "5.1k" V 7334 4175 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7380 4175 50  0001 C CNN
F 3 "~" H 7450 4175 50  0001 C CNN
F 4 "CR0603-JW-512ELF" V 7450 4175 50  0001 C CNN "MPN"
	1    7450 4175
	0    1    1    0   
$EndComp
$Comp
L Gateway-rescue:R-device R?
U 1 1 5DB487FD
P 7450 4275
AR Path="/5DB487FD" Ref="R?"  Part="1" 
AR Path="/5DAFB8E1/5DB487FD" Ref="R18"  Part="1" 
F 0 "R18" V 7243 4275 50  0000 C CNN
F 1 "5.1k" V 7334 4275 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7380 4275 50  0001 C CNN
F 3 "~" H 7450 4275 50  0001 C CNN
F 4 "CR0603-JW-512ELF" V 7450 4275 50  0001 C CNN "MPN"
	1    7450 4275
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7100 4275 7300 4275
Wire Wire Line
	7100 4175 7300 4175
Wire Wire Line
	7600 4175 7775 4175
Wire Wire Line
	7775 4175 7775 4275
Wire Wire Line
	7775 4275 7600 4275
Text Label 7775 4250 0    50   ~ 0
GND
NoConn ~ 7100 5175
NoConn ~ 7100 5075
NoConn ~ 6175 5475
Text Label 6500 5575 0    50   ~ 0
GND
Wire Wire Line
	6500 5575 6500 5475
Text Label 7250 4725 0    50   ~ 0
D+
Wire Wire Line
	7250 4725 7100 4725
Wire Wire Line
	7100 4725 7100 4775
Wire Wire Line
	7100 4675 7100 4725
Connection ~ 7100 4725
Text Label 7250 4525 0    50   ~ 0
D-
Wire Wire Line
	7250 4525 7100 4525
Wire Wire Line
	7100 4525 7100 4475
Wire Wire Line
	7100 4575 7100 4525
Connection ~ 7100 4525
$EndSCHEMATC
